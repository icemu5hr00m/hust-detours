#include <bits/stdc++.h>
#include <windows.h>
using namespace std;
#define LOGPATH "\\ApiLog.txt"



int main() {
    std::ifstream file(LOGPATH, std::ios::binary);  // 打开二进制文件

    if (!file) {
        std::cerr << "Open Failed！" << std::endl;
        return 1;
    }

    char c;
    while (file.get(c)) {  // 逐个读取字符
        std::cout << c;  // 输出到控制台
    }

    file.close();  // 关闭文件
    return 0;
}