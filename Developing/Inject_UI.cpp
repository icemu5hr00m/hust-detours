#include <iostream>
#include <cstdio>
#include <windows.h>
#include <detours.h>
#pragma comment(lib,"detours.lib")

//此处改为自己本地的地址
//#define DIRPATH L"D:\\VSIDE\\Detour project\\x64\\Debug"
//#define DLLPATH "D:\\VSIDE\\Detour project\\x64\\Debug\\Detour project.dll"
//#define EXEPATH L"D:\\VSIDE\\Detour project\\x64\\Debug\\APP.exe"

int main(int argc, char* argv[])
{
    //还未写有关传入参数的正确性的判断！其中参数1为DIRPATH，参数2为DLLPATH，参数3为EXEPATH
    wchar_t DIRPATH[256];
    //char DLLPATH[MAX_PATH+1];
    //sprintf(DLLPATH, "%ws", argv[2]);
    wchar_t EXEPATH[256];
    swprintf(DIRPATH, 256, L"%hs", argv[1]);
    swprintf(EXEPATH, 256, L"%hs", argv[3]);
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    ZeroMemory(&si, sizeof(STARTUPINFO));
    ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
    si.cb = sizeof(STARTUPINFO);
    WCHAR DirPath[MAX_PATH + 1];
    wcscpy_s(DirPath, MAX_PATH, DIRPATH);
    char DLLPath[MAX_PATH + 1];
    strcpy_s(DLLPath, MAX_PATH + 1, argv[2]);
    WCHAR EXE[MAX_PATH + 1] = { 0 };
    wcscpy_s(EXE, MAX_PATH, EXEPATH);

    if (DetourCreateProcessWithDllEx(EXE, NULL, NULL, NULL, TRUE, CREATE_DEFAULT_ERROR_MODE | CREATE_SUSPENDED, NULL, DirPath, &si, &pi, DLLPath, NULL))
    {
        std::cout << "Success!" << std::endl;
        ResumeThread(pi.hThread);
        WaitForSingleObject(pi.hProcess, INFINITE);
    }
    else
    {
        std::cout << "Failed!" << std::endl;
        char error[100];
        sprintf_s(error, "%d", GetLastError());
    }

    return 0;
}
