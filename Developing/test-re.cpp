﻿#include <windows.h>
#include <iostream>

int main() {
    LoadLibrary(L"C:\\Users\\zheny\\source\\repos\\test-reg\\x64\\Debug\\RegistryAnalysis.dll");

    HKEY hKey;
    // 测试路径，避免修改实际的启动项
    LPCWSTR subKey = L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\TestRegistryHook";
    LPCWSTR valueName = L"TestValue";
    DWORD data = 123;

    // 尝试创建一个注册表键
    if (RegCreateKeyEx(HKEY_CURRENT_USER, subKey, 0, NULL, 0, KEY_ALL_ACCESS, NULL, &hKey, NULL) == ERROR_SUCCESS) {
        std::cout << "Successfully created registry key." << std::endl;

        // 尝试设置一个值
        if (RegSetValueEx(hKey, valueName, 0, REG_DWORD, (const BYTE*)&data, sizeof(data)) == ERROR_SUCCESS) {
            std::cout << "Successfully set registry value." << std::endl;
        }

        // 尝试获取一个值
        DWORD type, cbData = sizeof(DWORD);
        if (RegGetValue(HKEY_CURRENT_USER, subKey, valueName, RRF_RT_REG_DWORD, &type, &data, &cbData) == ERROR_SUCCESS) {
            std::cout << "Successfully read registry value: " << data << std::endl;
        }

        // 尝试删除一个值
        if (RegDeleteValue(hKey, valueName) == ERROR_SUCCESS) {
            std::cout << "Successfully deleted registry value." << std::endl;
        }

        // 关闭并删除键
        RegCloseKey(hKey);
        if (RegDeleteKeyEx(HKEY_CURRENT_USER, L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run\\TestRegistryHook", KEY_WOW64_64KEY, 0) == ERROR_SUCCESS) {
            std::cout << "Successfully deleted registry key using RegDeleteKeyEx." << std::endl;
        }
    }
    else {
        std::cerr << "Failed to create registry key." << std::endl;
    }

    return 0;
}
