﻿#include <iostream>
#include <cstdio>
#include <windows.h>
#include <detours.h>
#include <fstream>
#include <filesystem>
#pragma comment(lib,"detours.lib")

//此处改为自己本地的地址
#define DIRPATH L"C:\\Users\\11839\\Desktop\\CSE_design\\codesrc\\dllmain\\x64\\Debug"
#define DLLPATH "DTdll.dll"
#define EXEPATH L"C:\\Users\\11839\\Desktop\\CSE_design\\codesrc\\APP\\x64\\Debug\\APP.exe"
#define LOGPATH "C:\\Users\\11839\\Desktop\\CSE_design\\codesrc\\dllmain\\x64\\Debug\\ApiLog.txt"

void logout()
{
    std::ifstream file(LOGPATH, std::ios::binary);  // 打开二进制文件

    if (!file) {
        std::cerr << "Open Failed！" << std::endl;
        return;
    }

    char c;
    while (file.get(c)) {  // 逐个读取字符
        std::cout << c;  // 输出到控制台
    }

    file.close();  // 关闭文件
}


int main(int argc, char* argv[])
{
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    ZeroMemory(&si, sizeof(STARTUPINFO));
    ZeroMemory(&pi, sizeof(PROCESS_INFORMATION));
    si.cb = sizeof(STARTUPINFO);


    WCHAR DirPath[MAX_PATH + 1];
    wcscpy_s(DirPath, MAX_PATH, DIRPATH);
    WCHAR EXE[MAX_PATH + 1];
    wcscpy_s(EXE, MAX_PATH, EXEPATH);

    if (DetourCreateProcessWithDllEx(EXE, NULL, NULL, NULL, TRUE, CREATE_DEFAULT_ERROR_MODE | CREATE_SUSPENDED, NULL, DirPath, &si, &pi, DLLPATH, NULL))
    {
        std::cout << "Success!" << std::endl;
        ResumeThread(pi.hThread);
        WaitForSingleObject(pi.hProcess, INFINITE);
    }
    else
    {
        std::cout << "Failed!" << std::endl;
        char error[100];
        sprintf_s(error, "%d", GetLastError());
    }

    logout();
    system("pause");

    return 0;
}
