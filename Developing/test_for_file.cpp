﻿#include <iostream>
#include <cstdio>
#include <windows.h>

int main()
{
	//关于file的测试代码
	//测试CreateFile
	//测试后缀名为.exe、.txt、.dll、.ocx文件的打开
	//同时检测打开或创建的文件所在的目录是否有多个文件夹
	HANDLE hFile = CreateFile(L"test_file.exe", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	HANDLE hFile2 = CreateFile(L"test_file.txt", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	HANDLE hFile3 = CreateFile(L"test_file.dll", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	HANDLE hFile4 = CreateFile(L"test_file.ocx", GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
	//Sleep(1000);

	//测试WriteFile
	//向四个打开的文件写入同样的数据："It's a test file."
	const char* filecontent = "It's a test file.";
	WCHAR FILECONTENT[256];
	memset(FILECONTENT, 0, sizeof(FILECONTENT));
	MultiByteToWideChar(CP_ACP, 0, filecontent, strlen(filecontent) + 1, FILECONTENT, sizeof(FILECONTENT) / sizeof(FILECONTENT[0]));
	DWORD BytesWritten;
	if (!WriteFile(hFile, filecontent, strlen(filecontent), &BytesWritten, NULL)) {
		std::cout << ".exe file writing  failed." << std::endl;
		return 1;
	}
	std::cout << ".exe file writing  succeeded." << std::endl;
	if (!WriteFile(hFile2, filecontent, strlen(filecontent), &BytesWritten, NULL)) {
		std::cout << ".txt file writing  failed." << std::endl;
		return 1;
	}
	std::cout << ".txt file writing  succeeded." << std::endl;
	if (!WriteFile(hFile3, filecontent, strlen(filecontent), &BytesWritten, NULL)) {
		std::cout << ".dll file writing  failed." << std::endl;
		return 1;
	}
	std::cout << ".dll file writing  succeeded." << std::endl;
	if (!WriteFile(hFile4, filecontent, strlen(filecontent), &BytesWritten, NULL)) {
		std::cout << ".ocx file writing  failed." << std::endl;
		return 1;
	}
	std::cout << ".ocx file writing  succeeded." << std::endl;


	//测试ReadFile
	char buffer[256];
	DWORD BytesRead;
	SetFilePointer(hFile, NULL, NULL, FILE_BEGIN);
	ReadFile(hFile, buffer, 255, &BytesRead, NULL);
	buffer[BytesRead] = '\0';
	std::cout << "File Content:\n" << buffer << std::endl;//思路有点问题，不该在测试文件中输出，还在尝试修改使API可以输出文件内容

	//测试CloseHandle  
	//会额外输出很多关闭其他文件的信息，在调试时建议把Dll的main函数中CloseHandle注释掉
	CloseHandle(hFile);
	CloseHandle(hFile2);
	CloseHandle(hFile3);
	CloseHandle(hFile4);



	//文件异常分析之文件自我复制CopyFile
	//测试时先在本地创建文件test1.txt，日志中可以输出对应信息
	CopyFile(L"test1.txt", L"test2.txt", FALSE);
	//查找操作范围是否还有目录，在CfreateFile中实现；测试修改其他文件在WriteFile中实现

	//测试DeleteFile  
	//调用后会删除文件，调试时不建议使用
	DeleteFile(L"test_file.txt");
	DeleteFile(L"test_file.exe");
	DeleteFile(L"test_file.dll");
	DeleteFile(L"test_file.ocx");

	return 0;
}