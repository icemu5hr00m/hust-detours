#include "pch.h"
#include "framework.h"
#include "detours.h"
#include "stdio.h"
#include "stdarg.h"
#include <windows.h>
#include <detours.h>
#include <chrono>
#include <iomanip>
#include <sstream>
#include <string>
#include <map>
#include <iostream>
#define _CRT_SECURE_NO_WARNINGS
#pragma comment(lib, "detours.lib")

SYSTEMTIME st;

// 原始函数指针
static LSTATUS(WINAPI* OldRegCreateKeyEx)(_In_ HKEY hkey,
    _In_ LPCWSTR lpSubKey,
    _In_ DWORD Reserved,
    _In_opt_ LPWSTR lpClass,
    _In_ DWORD dwOptions,
    _In_ REGSAM samDesired,
    _In_opt_ const LPSECURITY_ATTRIBUTES lbSecurityAttributes,
    _Out_ PHKEY phkResult,
    _Out_opt_ LPDWORD lpdwDisposition) = RegCreateKeyEx;

static LSTATUS(WINAPI* OldRegDeleteKeyEx)(_In_ HKEY hkey,
    _In_opt_ LPCWSTR lpSubKey,
    _In_ REGSAM samDesired,
    _In_ DWORD Reserved) = RegDeleteKeyEx;

static LSTATUS(WINAPI* OldRegSetValueEx)(_In_ HKEY hkey,
    _In_opt_ LPCWSTR lpValueName,
    _In_opt_ DWORD Reserved,
    _In_ DWORD dwType,
    _In_ const BYTE* lpData,
    _In_ DWORD cbData) = RegSetValueEx;

static LSTATUS(WINAPI* OldRegGetValue)(_In_ HKEY hkey,
    _In_opt_ LPCWSTR lpSubKey,
    _In_opt_ LPCWSTR lpValue,
    _In_opt_ DWORD dwFlags,
    _Out_opt_ LPDWORD pdwType,
    _Out_opt_ PVOID pvData,
    _Inout_opt_ LPDWORD pcbData) = RegGetValue;

// 用于替代原始函数的新函数
extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegCreateKeyEx(
    _In_ HKEY hkey,
    _In_ LPCWSTR lpSubKey,
    _In_ DWORD Reserved,
    _In_opt_ LPWSTR lpClass,
    _In_ DWORD dwOptions,
    _In_ REGSAM samDesired,
    _In_opt_ const LPSECURITY_ATTRIBUTES lbSecurityAttributes,
    _Out_ PHKEY phkResult,
    _Out_opt_ LPDWORD lpdwDisposition);

extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegDeleteKeyEx(
    _In_ HKEY hkey,
    _In_opt_ LPCWSTR lpSubKey,
    _In_ REGSAM samDesired,
    _In_ DWORD Reserved);

extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegSetValueEx(
    _In_ HKEY hkey,
    _In_opt_ LPCWSTR lpValueName,
    _In_opt_ DWORD Reserved,
    _In_ DWORD dwType,
    _In_ const BYTE * lpData,
    _In_ DWORD cbData);

extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegGetValue(
    _In_ HKEY hkey,
    _In_opt_ LPCWSTR lpSubKey,
    _In_opt_ LPCWSTR lpValue,
    _In_opt_ DWORD dwFlags,
    _Out_opt_ LPDWORD pdwType,
    _Out_opt_ PVOID pvData,
    _Inout_opt_ LPDWORD pcbData);

// 获取当前进程名称的实用函数
std::wstring GetCurrentProcessName() {
    WCHAR buffer[MAX_PATH] = { 0 };
    GetModuleFileNameW(NULL, buffer, MAX_PATH);
    return std::wstring(buffer);
}

// 获取当前时间字符串的实用函数
std::wstring GetCurrentTimeWStr() {
    auto now = std::chrono::system_clock::now();
    auto in_time_t = std::chrono::system_clock::to_time_t(now);
    std::tm tm;
    localtime_s(&tm, &in_time_t);
    std::wstringstream ss;
    ss << std::put_time(&tm, L"%Y-%m-%d %X");
    return ss.str();
}

// 向控制台输出消息
void OutputToConsole(const std::wstring& message) {
    std::wcout << message << std::endl;
}

// 使用时间戳记录注册表操作
void LogRegistryOperation(const std::wstring& message) {
    std::wstring currentTime = GetCurrentTimeWStr();
    std::wstring logMessage = L"[" + currentTime + L"] " + message;
    OutputToConsole(logMessage);
}

// 新的 NewRegCreateKeyEx 函数，记录尝试创建注册表键的操作
extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegCreateKeyEx(
    _In_ HKEY hkey,
    _In_ LPCWSTR lpSubKey,
    _In_ DWORD Reserved,
    _In_opt_ LPWSTR lpClass,
    _In_ DWORD dwOptions,
    _In_ REGSAM samDesired,
    _In_opt_ const LPSECURITY_ATTRIBUTES lbSecurityAttributes,
    _Out_ PHKEY phkResult,
    _Out_opt_ LPDWORD lpdwDisposition) {

    GetLocalTime(&st);
    std::wstring processName = GetCurrentProcessName();
    std::wstring message = processName + L" attempts to create registry key: " + (lpSubKey ? lpSubKey : L"<undefined>");
    LogRegistryOperation(message);
    std::wstring subKeyStr(lpSubKey ? lpSubKey : L"");
    OutputToConsole(subKeyStr);
    if ((hkey == HKEY_LOCAL_MACHINE && subKeyStr.find(L"Software\\Microsoft\\Windows\\CurrentVersion\\Run") != std::wstring::npos) ||
        (hkey == HKEY_CURRENT_USER && subKeyStr.find(L"Software\\Microsoft\\Windows\\CurrentVersion\\Run") != std::wstring::npos)) {
        std::wstring message = L"Attempt to create an autostart entry: " + subKeyStr;
        LogRegistryOperation(message);
    }

    return OldRegCreateKeyEx(hkey, lpSubKey, Reserved, lpClass, dwOptions, samDesired, lbSecurityAttributes, phkResult, lpdwDisposition);
}

// 新的 NewRegDeleteKeyEx 函数，记录尝试删除注册表键的操作
extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegDeleteKeyEx(
    _In_ HKEY hkey,
    _In_opt_ LPCWSTR lpSubKey,
    _In_ REGSAM samDesired,
    _In_ DWORD Reserved) {

    GetLocalTime(&st);
    std::wstring processName = GetCurrentProcessName();
    std::wstring message = processName + L" attempts to delete registry key: " + (lpSubKey ? lpSubKey : L"<undefined>");
    LogRegistryOperation(message);

    return OldRegDeleteKeyEx(hkey, lpSubKey, samDesired, Reserved);
}

// 新的 NewRegSetValueEx 函数，记录尝试设置注册表值的操作
extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegSetValueEx(
    _In_ HKEY hkey,
    _In_opt_ LPCWSTR lpValueName,
    _In_opt_ DWORD Reserved,
    _In_ DWORD dwType,
    _In_ const BYTE * lpData,
    _In_ DWORD cbData) {

    GetLocalTime(&st);
    std::wstring processName = GetCurrentProcessName();
    std::wstring message = processName + L" attempts to set registry value: " + (lpValueName ? lpValueName : L"<undefined>");
    LogRegistryOperation(message);
    WCHAR currentKeyName[256];
    DWORD size = sizeof(currentKeyName);
    if (RegQueryInfoKey(hkey, currentKeyName, &size, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL) == ERROR_SUCCESS) {
        std::wstring keyName(currentKeyName);
        if (keyName.find(L"Software\\Microsoft\\Windows\\CurrentVersion\\Run") != std::wstring::npos) {
            std::wstring valueNameStr(lpValueName ? lpValueName : L"");
            std::wstring message = L"Attempt to modify an autostart entry: " + valueNameStr;
            LogRegistryOperation(message);
        }
    }

    return OldRegSetValueEx(hkey, lpValueName, Reserved, dwType, lpData, cbData);
}

// 新的 NewRegGetValue 函数，记录尝试获取注册表值的操作
extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegGetValue(
    _In_ HKEY hkey,
    _In_opt_ LPCWSTR lpSubKey,
    _In_opt_ LPCWSTR lpValue,
    _In_opt_ DWORD dwFlags,
    _Out_opt_ LPDWORD pdwType,
    _Out_opt_ PVOID pvData,
    _Inout_opt_ LPDWORD pcbData) {

    GetLocalTime(&st);
    std::wstring processName = GetCurrentProcessName();
    std::wstring message = processName + L" attempts to get registry value: " + (lpValue ? lpValue : L"<undefined>");
    LogRegistryOperation(message);

    return OldRegGetValue(hkey, lpSubKey, lpValue, dwFlags, pdwType, pvData, pcbData);
}

// DllMain 处理 DLL 的加载和卸载
BOOL WINAPI DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved) {
    switch (ul_reason_for_call) {
    case DLL_PROCESS_ATTACH:
        DisableThreadLibraryCalls(hModule);
        DetourTransactionBegin();
        DetourUpdateThread(GetCurrentThread());
        // 附加钩子
        DetourAttach(&(PVOID&)OldRegCreateKeyEx, NewRegCreateKeyEx);
        DetourAttach(&(PVOID&)OldRegDeleteKeyEx, NewRegDeleteKeyEx);
        DetourAttach(&(PVOID&)OldRegSetValueEx, NewRegSetValueEx);
        DetourAttach(&(PVOID&)OldRegGetValue, NewRegGetValue);
        DetourTransactionCommit();
        break;
    case DLL_PROCESS_DETACH:
        DetourTransactionBegin();
        DetourUpdateThread(GetCurrentThread());
        // 分离钩子
        DetourDetach(&(PVOID&)OldRegCreateKeyEx, NewRegCreateKeyEx);
        DetourDetach(&(PVOID&)OldRegDeleteKeyEx, NewRegDeleteKeyEx);
        DetourDetach(&(PVOID&)OldRegSetValueEx, NewRegSetValueEx);
        DetourDetach(&(PVOID&)OldRegGetValue, NewRegGetValue);
        DetourTransactionCommit();
        break;
    }
    return TRUE;
}
