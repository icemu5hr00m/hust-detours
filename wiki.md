<!-- [TOC] -->

- [使用指南](#使用指南)
  - [概览](#概览)
  - [使用方法](#使用方法)
  - [运行示例](#运行示例)
  - [支持的系统 API](#支持的系统-api)
  - [支持的异常行为](#支持的异常行为)
- [实现原理](#实现原理)
  - [API 捕获原理](#api-捕获原理)
  - [异常行为分析原理](#异常行为分析原理)
- [杂项](#杂项)
  - [添加自己的 API](#添加自己的-api)

---
#   使用指南
---
##  概览
本程序采用微软的开源工具库 Detours 实现 Windows API 截获（或称为绕道、挂钩）操作，并输出 API 相关调用信息，从而可以对样本程序的基本分析。关于 Detours 请参阅 [Detours Wiki](https://github.com/microsoft/Detours/wiki) 来了解更多信息。

##  使用方法
- 1. 在以下目录找到本程序的本体 `...\FinalVersion\UI_src\UI.exe` ，打开后将显示如下界面：![](./wiki_pic/1.png)

  2. 然后在DLL文件一栏选择路径到附带的 `DTdll.dll` 的所在位置，默认位置应该在 `...\FinalVersion\DTdll.dll`

  3. 最后选择要测试的样本程序，默认附带一些简单的样本在目录 `...\FinalVersion\samples` 下

  4. 单击开始分析即可

##  运行示例
下面以附带的样本程序 `Heaptest.exe` 为例介绍具体的运行实例：

- 以下是 `Heaptest.exe` 的源码，在目录 `...\FinalVersion\samples\codesrc` 可以找到

```cpp
#include <iostream>
#include <cstdio>
#include <windows.h>

int main()
{
    HANDLE hHeap1 = HeapCreate(0, 0, 0);
    HeapDestroy(hHeap1);

    HANDLE hHeap = HeapCreate(0, 0, 0);
    LPVOID lp1 = HeapAlloc(hHeap, 0, 10);

    BOOL bb = HeapFree(hHeap, 0, lp1);
    HeapFree(hHeap, 0, NULL);

    //HeapFree(hHeap, 0, lp1);

    HeapDestroy(hHeap);

    return 0;
}
```

- 观察源码可以发现其有一处堆释放有问题，即 `HeapFree(hHeap, 0, NULL);` 企图释放一个不是通过 `HeapAlloc` 获取的指针[^1]

- 选择文件路径并开始分析![](./wiki_pic/2.png)

- 分析后会出现此次分析的结果窗口![](./wiki_pic/3.png)
  
- 或者打开 `\FinalVersion\UI_src\ApiLog.txt` 通过输出的日志文件来查看分析结果![](./wiki_pic/4.png)

- 可以观察到在最后的输出中有提示信息 `ERROR! An unallocated pointer 0 has been freed!` 代表本程序成功发现该测试程序的堆释放漏洞

##  支持的系统 API
本程序支持对以下系统 API 的调用信息进行捕获：
| 系统 API | 记录的信息 |
| ---- | ---- |
| [*MessageBoxW*](https://learn.microsoft.com/zh-cn/windows/win32/api/winuser/nf-winuser-messageboxw) | 窗口句柄、消息文本、标题文本、消息类型 |
| [*MessageBoxA*](https://learn.microsoft.com/zh-cn/windows/win32/api/winuser/nf-winuser-messageboxa) | 窗口句柄、消息文本、标题文本、消息类型 |
| [*CreateFile*](https://learn.microsoft.com/zh-cn/windows/win32/api/fileapi/nf-fileapi-createfilea) | 文件名称、访问方式、共享方式、安全描述符、创建方式、文件属性、模板句柄 |
| [*DeleteFile*](https://learn.microsoft.com/zh-cn/windows/win32/api/fileapi/nf-fileapi-deletefilea) | 文件名称 |
| [*ReadFile*](https://learn.microsoft.com/zh-cn/windows/win32/api/fileapi/nf-fileapi-readfile) | 文件句柄、读取缓冲区位置、读取长度、实际读取长度、异步参数 |
| [*WriteFile*](https://learn.microsoft.com/zh-cn/windows/win32/api/fileapi/nf-fileapi-writefile) | 文件句柄、写入缓冲区位置、写入长度、实际写入长度、异步参数 |
| [*CloseHandle*](https://learn.microsoft.com/zh-cn/windows/win32/api/handleapi/nf-handleapi-closehandle) | 对象句柄 |
| [*CopyFile*](https://learn.microsoft.com/zh-cn/windows/win32/api/winbase/nf-winbase-copyfile) | 原文件名、新文件名 |
| [*HeapCreate*](https://learn.microsoft.com/zh-cn/windows/win32/api/heapapi/nf-heapapi-heapcreate) | 堆行为、堆初始大小、堆最大大小、堆句柄 |
| [*HeapDestroy*](https://learn.microsoft.com/zh-cn/windows/win32/api/heapapi/nf-heapapi-heapdestroy) | 堆句柄 |
| [*HeapAlloc*](https://learn.microsoft.com/zh-cn/windows/win32/api/heapapi/nf-heapapi-heapalloc) | 堆句柄、分配行为、分配大小、分配位置 |
| [*HeapFree*](https://learn.microsoft.com/zh-cn/windows/win32/api/heapapi/nf-heapapi-heapfree) | 堆句柄、释放行为、释放位置 |
| [*RegCreateKeyEx*](https://learn.microsoft.com/zh-cn/windows/win32/api/winreg/nf-winreg-regcreatekeyexa) | HKEY句柄、子项名称、保留字、值名称、创建方式、访问方式、安全描述符位置、接收位置、接收值 |
| [*RegDeleteKeyEx*](https://learn.microsoft.com/zh-cn/windows/win32/api/winreg/nf-winreg-regdeletekeyexa) | HKEY句柄、子项名称、访问权限、保留字 |
| [*RegSetValueEx*](https://learn.microsoft.com/zh-cn/windows/win32/api/winreg/nf-winreg-regsetvalueexa) | HKEY句柄、设置值名称、保留字、值类型、设置值数据、数据大小 |
| [*RegGetValue*](https://learn.microsoft.com/zh-cn/windows/win32/api/winreg/nf-winreg-reggetvaluea) | HKEY句柄、子项名称、值名称、获取方式、接收位置、存储缓冲区位置、占用大小 |

且所有捕获均会额外记录 API 名和调用时间

##  支持的异常行为
本程序支持对调用以下 API 时存在的异常行为信息进行捕获：
| 系统 API | 异常行为 |
| ---- | ---- |
| CreateFile | 操作范围有多个文件夹 |
| WriteFile | 修改其他可执行文件 |
| CopyFile | 存在复制操作 |
| HeapFree | 堆释放位置和申请位置不一致 |
| HeapFree | 存在多重释放操作 |
| RegCreateKeyEx | 添加自启动的注册表 |
| RegSetValueEx | 修改自启动的注册表 |

---
#   实现原理
---
##  API 捕获原理
相关内容请参见 [Detours Wiki](https://github.com/microsoft/Detours/wiki) 中的有关介绍：["Interception of Binary Functions"](https://github.com/microsoft/Detours/wiki/OverviewInterception)

##  异常行为分析原理
- **操作范围有多个文件夹**
当被分析的样本程序（后文仅称“样本”）调用 `CreateFile` 时，将通过返回的文件句柄使用 `GetFinalPathNameByHandle` 函数得到操作路径，然后在该路径遍历文件名，并检查是否存在多个目录，若存在，则输出警告信息到日志

- **修改其他可执行文件**
当样本调用 `WriteFile` 时，通过传入参数检查要修改的文件后缀是否是 `.exe`、`.dll`、`.ocx` 等常规可执行文件的后缀，若是则输出警告信息和样本具体修改的文件名到日志

- **存在复制操作**
当样本调用 `CopyFile` 时就会警告，且将被复制和新复制的文件名输出在日志中[^2]

- **堆释放位置和申请位置不一致**
每当样本进行 `HeapAlloc` 操作时，程序会记录申请到的指针位置；当样本进行 `HeapFree` 操作时，会根据释放的指针位置寻找是否有对应的记录，若无记录则输出错误信息到日志

- **存在多重释放操作**
每当样本进行 `HeapFree` 操作后，会根据释放的指针位置进行记录；当样本再次进行 `HeapFree` 操作时，若发现记录的指针的释放次数大于申请获得的次数，则会输出错误信息到日志

- **添加自启动的注册表**
当样本调用 `RegCreateKeyEx` 时，会根据传入参数，检查创建的具体注册表位置，若为和系统开机自启动表项相关，则会输出警告信息到日志中，且会开始监视这个表项的进一步操作

- **修改自启动的注册表**
当样本调用 `RegSetValueEx` 时，会检查其修改的注册表，是否为刚刚在调用 `RegCreateKeyEx` 时被添加到程序监视列表中的注册表项，若是则会输出警告信息到日志中

---
#   杂项
---
##  添加自己的 API
**注意！以下内容是针对对本程序有一定了解，且想要自行修改以适应自身需求的高级用户和开发人员所撰写的，用于介绍如何自定义添加 API**

添加自定义的系统 API 捕获可以通过以下几个对 `dllmain.cpp` 的修改步骤实现：

1. 添加一个关联 API 函数指针的静态变量（以下称为 `OldAPI` ）；
2. 定义捕获函数并以 C 格式导出（以下称为 `NewAPI` ）；
3. 将 `OldAPI` 和 `NewAPI` 加入到 `DetourAttach` 和 `DetourDetach` 中。

这里以 `MessageBoxA` API 为例，介绍用户如何添加自己想捕获的 API ：

- 首先声明一个与要捕获 API 类型相同的静态函数指针，并初始化为要捕获 API 函数指针的值，即 `OldAPI`

```cpp
static int (WINAPI* OldMessageBoxA)(_In_opt_ HWND hWnd, _In_opt_ LPCSTR lpText, _In_opt_ LPCSTR lpCaption, _In_ UINT uType) = MessageBoxA;
```

- 然后声明 `NewAPI` 的接口

```cpp
extern "C" __declspec(dllexport)int WINAPI NewMessageBoxA(_In_opt_ HWND hWnd, _In_opt_ LPCSTR lpText, _In_opt_ LPCSTR lpCaption, _In_ UINT uType);
```

- 并根据自身需求对捕获函数进行合适定义。例如对 API 调用时间进行捕获，以及设计一些逻辑以分析异常行为

```cpp
... NewMessageBoxA(...) {
    printf("MessageBoxA Hooked\n");
        GetLocalTime(&st);
        printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
}
```

除了可以像以上直接输出到输出缓冲区，还可以使用内置的 `apilog` 进行缓存并最后一并输出到日志中

```cpp
... NewMessageBoxA(...) {
    strcpy_s(apilog.type, "MessageBoxA\n");
        apilog.argNum = 4;
        GetLocalTime(&(apilog.st));

        sprintf_s(apilog.argName[0], "hWnd:");
        sprintf_s(apilog.argName[1], "lpText:");
        sprintf_s(apilog.argName[2], "lpCaption:");
        sprintf_s(apilog.argName[3], "uType:");
        // 参数值
        sprintf_s(apilog.argVal[0], "%llx\n", hWnd);
        sprintf_s(apilog.argVal[1], "%s\n", lpText);
        sprintf_s(apilog.argVal[2], "%s\n", lpCaption);
        sprintf_s(apilog.argVal[3], "%llx\n", uType);
        WriteLog();
}
```

**最重要的一步**是记得使用 `OldAPI` 作为 `Trampoline` 函数返回到原来的 API ，并将参数按原样传入

```cpp
... NewMessageBoxA(...) {
    return OldMessageBoxA(NULL, "new MessageBoxA", "Hooked", MB_OK);
}
```

- 在 `Attach_All` 和 `Detach_All` 中加入新的捕获

```cpp
void Attach_All() {
    DetourAttach(&(PVOID&)OldMessageBoxA, NewMessageBoxA);
}
```

```cpp
void Detach_All() {
    DetourDetach(&(PVOID&)OldMessageBoxA, NewMessageBoxA);
}
```

通过以上添加后，重新将 `dllmain.cpp` 编译成 DLL 文件，再次使用程序时，DLL 路径选择为新 DLL 文件的路径，即可捕获用户自己设定的系统 API 调用了



[^1]: 此处使用空指针用作实验，因此对系统不会产生实际影响。
[^2]: 未来可能会改进成如果传入的参数为样本自身才进行警告，仅用于监测复制自身。