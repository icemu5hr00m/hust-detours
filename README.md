# 课程设计任务要求

## 实现基本的第三方进程WindowsAPI截获框架

框架包括1.1编译生成Detours库；1.2完成挂钩框架DLL，实现对MessageBox调用截获，能打印出调用的参数、进程名称以及进程Exe文件信息；1.3自编或者利用已有恶意代码样例（包含弹出对话框动作）；1.4完成注入动作开启和关闭的“注射器”控制程序

## 实现堆操作API截获

修改1.2-1.4，实现堆操作（创建，释放）API进行截获，打印出所有参数信息。

## 实现文件操作API截获

实现对文件操作（创建，关闭，读写）API进行截获，打印出所有参数信息。

## 注册表操作API截获

实现对注册表操作（创建，关闭，读写）API进行截获，打印出所有参数信息。

## 堆操作异常行为分析

设计并完成算法，记录并给出提示：
1. 检测堆申请与释放是否一致（正常）；
2. 是否发生重复的多次释放（异常）

## 文件操作异常行为分析

设计并完成算法，记录并给出提示：
1.  判断操作范围是否有多个文件夹；
2.  是否存在自我复制的情况；
3.  是否修改了其它可执行代码包括exe，dll，ocx等；
4.  是否将文件内容读取后发送到网络（选做）
## 注册表操作异常行为分析

设计并完成算法，记录并给出提示：
1.  判断是否新增注册表项并判断是否为自启动执行文件项；
2.  是否修改了注册表；
3.  输出所有的注册表操作项；

## 提供系统界面

所设计实现的功能，有图形界面展示

## 行为检测样本库

提供5个待检测的可能存在恶意的Exe样本

## 项目开源

在Gitee平台上开源各自完成的行为分析系统，小组成员通过Gitee协同开发

## 项目文档撰写

在开源项目中编写项目构建与使用文档

## 在指定的开源仓库完成开源贡献

在以下开源仓库中提交PR：
1. Gitee的HustDetours（待公布具体网址）
2. 软件安全课程平台的基础架构仓库（[https://github.com/HUSTSeclab/dojo](https://github.com/HUSTSeclab/dojo)）
3. 理论课实验课实验仓库([https://github.com/HUSTSeclab/software-security-dojo](https://github.com/HUSTSeclab/software-security-dojo))

## 网络通信操作异常行为分析（选做）

设计并完成算法，记录并给出提示：
1. 实现对网络传输SOCKET操作（连接、发送与接收）API的截获；
2. 打印进程连接端口、协议类型、IP信息
3. HTTP连接协议的解析，判断传输的内容是否为明文

## 内存拷贝监测与关联分析（选做）

设计并完成算法，记录并给出提示：
	能够输出内存拷贝信息，并分析拷贝的内容流向。













---
# Microsoft Research Detours Package

Detours is a software package for monitoring and instrumenting API calls on Windows. Detours
has been used by many ISVs and  is also  used by product teams at Microsoft. Detours is now available under
a standard open source  license ([MIT](https://github.com/microsoft/Detours/blob/master/LICENSE.md)).  This simplifies licensing for programmers using Detours
and allows the community to support Detours using open source tools and processes.

Detours is compatible with the Windows NT family of 
operating systems: Windows NT, Windows XP, Windows Server 2003, Windows 7,
Windows 8, and Windows 10.  It cannot be used by Windows Store apps
because Detours requires APIs not available to those applications. 
This repo contains the source code for version 4.0.1 of Detours.

For technical documentation on Detours, see the [Detours Wiki](https://github.com/microsoft/Detours/wiki).
For directions on how to build and run samples, see the
samples [README.txt](https://github.com/Microsoft/Detours/blob/master/samples/README.TXT) file.

## Contributing

The [`Detours`](https://github.com/microsoft/detours) repository is where development is done.
Here are some ways you can participate in the project:

* [Answer questions](https://github.com/microsoft/detours/issues) about using Detours.
* [Improve the Wiki](https://github.com/microsoft/detours/wiki).
* [Submit bugs](https://github.com/microsoft/detours/issues) and help us verify fixes and changes as they are checked in.
* Review [source code changes](https://github.com/microsoft/detours/pulls).

Most contributions require you to agree to a Contributor License Agreement (CLA) declaring that
you have the right to, and actually do, grant us the rights to use your contribution.
For details, visit https://cla.opensource.microsoft.com.

When you submit a pull request, a CLA bot will automatically determine whether you need to provide
a CLA and decorate the PR appropriately (e.g., status check, comment). Simply follow the instructions
provided by the bot. You will only need to do this once across all repos using our CLA.

This project has adopted the [Microsoft Open Source Code of Conduct](https://opensource.microsoft.com/codeofconduct/). For more information see the [Code of Conduct FAQ](https://opensource.microsoft.com/codeofconduct/faq/) or contact [opencode@microsoft.com](mailto:opencode@microsoft.com) with any additional questions or comments.

## Issues, questions, and feedback

* Open an issue on [GitHub Issues](https://github.com/Microsoft/detours/issues).

## Mailing list for announcements

The detours-announce mailing list is a low-traffic email list for important announcements 
about the project, such as the availability of new versions of Detours.  To join it, send 
an email to listserv@lists.research.microsoft.com with a 
message body containing only the text SUBSCRIBE DETOURS-ANNOUNCE.
To leave it, send an email to listserv@lists.research.microsoft.com with a 
message body containing only the text UNSUBSCRIBE DETOURS-ANNOUNCE.


## License

Copyright (c) Microsoft Corporation. All rights reserved.

Licensed under the [MIT](LICENSE.md) License.
