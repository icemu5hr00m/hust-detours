﻿#include "pch.h"
#include "framework.h"
#include "detours.h"
#include "stdio.h"
#include "stdarg.h"
#include "windows.h"
#include <iostream>
#include <bits/stdc++.h>
#include <filesystem>
#include <pathcch.h>
#pragma comment(lib,"detours.lib")
#pragma comment(lib,"Pathcch.lib")
#define MAX_PATH 100


#pragma data_seg("Shared")        // 声明共享数据段，并命名该数据段
int Share_Flag = 1;
#pragma data_seg()
#pragma comment(linker,"/section:.Shared,rws")  // 可读，可写，进程间共享。所有加载此dll的进程共享一份内存

SYSTEMTIME st;
HANDLE hFile0 = CreateFile(L"ApiLog.txt", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);//文件与injector.exe在同一目录
std::map<HKEY, int>danger_hkey;
std::map<HANDLE, int> hHeapMap;
std::map<LPVOID, int> Alloc_Addr_Map;
std::map<LPVOID, int> Freed_Addr_Map;
std::map<HANDLE, int> File_Map;
LPCSTR pszBuf;

struct ApiLog
{
    char type[100];//hook函数名
    int argNum;//参数数量
    SYSTEMTIME st;//时间
    char argName[10][30] = { 0 };//参数名
    char argVal[10][70] = { 0 };//参数值
    char errormsg[100] = { 0 };
    bool err = 0;
};
ApiLog apilog;

HANDLE FileCreated[100];//记录经过CreateFile生成的文件句柄
int Filenum = 0;//记录FileCreated个数

static int (WINAPI* OldMessageBoxW)(_In_opt_ HWND hWnd, _In_opt_ LPCWSTR lpText, _In_opt_ LPCWSTR lpCaption, _In_ UINT uType) = MessageBoxW;
static int (WINAPI* OldMessageBoxA)(_In_opt_ HWND hWnd, _In_opt_ LPCSTR lpText, _In_opt_ LPCSTR lpCaption, _In_ UINT uType) = MessageBoxA;
static HANDLE(WINAPI* OldCreateFile)(LPCTSTR lpFileName, DWORD dwDesireAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecutrity_attributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile) = CreateFile;
static BOOL(WINAPI* OldDeleteFile)(LPCTSTR lpFileName) = DeleteFile;
static BOOL(WINAPI* OldReadFile)(HANDLE hFile, LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead, LPOVERLAPPED lpOverlapped) = ReadFile;
static BOOL(WINAPI* OldWriteFile)(HANDLE hFile, LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten, LPOVERLAPPED lpOverlapped) = WriteFile;
static BOOL(WINAPI* OldCopyFile)(LPCTSTR lpExistingFileName, LPCTSTR lpNewFileName, BOOL bFailIfExists) = CopyFile;
static BOOL(WINAPI* OldCloseHandle)(HANDLE hObject) = CloseHandle;
static HANDLE(WINAPI* OldHeapCreate)(DWORD fIOoptions, SIZE_T dwInitialSize, SIZE_T dwMaximumSize) = HeapCreate;
static BOOL(WINAPI* OldHeapDestroy)(HANDLE hHeap) = HeapDestroy;
static LPVOID(WINAPI* OldHeapAlloc)(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes) = HeapAlloc;
static BOOL(WINAPI* OldHeapFree)(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem) = HeapFree;
static LSTATUS(WINAPI* OldRegCreateKeyEx)(_In_ HKEY hkey,
    _In_ LPCWSTR lpSubKey,
    _In_ DWORD Reserved,
    _In_opt_ LPWSTR lpClass,
    _In_ DWORD dwOptions,
    _In_ REGSAM samDesired,
    _In_opt_ const LPSECURITY_ATTRIBUTES lbSecurityAttributes,
    _Out_ PHKEY phkResult,
    _Out_opt_ LPDWORD lpdwDisposition) = RegCreateKeyEx;    //关于创建注册表项的API
static LSTATUS(WINAPI* OldRegDeleteKeyEx)(_In_ HKEY hkey, _In_opt_ LPCWSTR lpSubKey, _In_ REGSAM samDesired, _In_ DWORD Reserved) = RegDeleteKeyEx;    //关于删除注册表项的API
//关于设定特定注册表项值RegSetValueEx
static LSTATUS(WINAPI* OldRegSetValueEx)(_In_ HKEY hkey, _In_opt_ LPCWSTR lpValueName, _In_opt_ DWORD Reserved, _In_ DWORD dwType, _In_ const BYTE* lpData, _In_ DWORD cbData) = RegSetValueEx;
//关于查询指定注册表值的类型和数据
static LSTATUS(WINAPI* OldRegGetValue)(_In_ HKEY hkey, _In_opt_ LPCWSTR lpSubKey, _In_opt_ LPCWSTR lpValue, _In_opt_ DWORD dwFlags, _Out_opt_ LPDWORD pdwType, _Out_opt_ PVOID pvData, _Inout_opt_ LPDWORD pcbData) = RegGetValue;

extern "C" __declspec(dllexport)int WINAPI NewMessageBoxA(_In_opt_ HWND hWnd, _In_opt_ LPCSTR lpText, _In_opt_ LPCSTR lpCaption, _In_ UINT uType);
extern "C" __declspec(dllexport)int WINAPI NewMessageBoxW(_In_opt_ HWND hWnd, _In_opt_ LPCWSTR lpText, _In_opt_ LPCWSTR lpCaption, _In_ UINT uType);
extern "C" __declspec(dllexport)HANDLE WINAPI NewCreateFile(LPCTSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile);
extern "C" __declspec(dllexport)BOOL WINAPI NewDeleteFile(LPCTSTR lpFileName);
extern "C" __declspec(dllexport)BOOL WINAPI NewReadFile(HANDLE hFile, LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead, LPOVERLAPPED lpOverlapped);
extern "C" __declspec(dllexport)BOOL WINAPI NewWriteFile(HANDLE hFile, LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten, LPOVERLAPPED lpOverlapped);
extern "C" _declspec(dllexport)BOOL WINAPI NewCloseHandle(HANDLE hObject);
extern "C" _declspec(dllexport)BOOL WINAPI NewCopyFile(LPCTSTR lpExistingFileName, LPCTSTR lpNewFileName, BOOL bFailIfExists);
extern "C" __declspec(dllexport)HANDLE WINAPI NewHeapCreate(DWORD fIOoptions, SIZE_T dwInitialSize, SIZE_T dwMaximumSize);
extern "C" __declspec(dllexport)BOOL WINAPI NewHeapDestroy(HANDLE hHeap);
extern "C" __declspec(dllexport)LPVOID WINAPI NewHeapAlloc(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes);
extern "C" __declspec(dllexport)BOOL WINAPI NewHeapFree(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem);
extern "C" __declspec(dllexport)  LSTATUS WINAPI NewRegCreateKeyEx(
    _In_ HKEY hkey,
    _In_ LPCWSTR lpSubKey,
    _In_ DWORD Reserved,
    _In_opt_ LPWSTR lpClass,
    _In_ DWORD dwOptions,
    _In_ REGSAM samDesired,
    _In_opt_ const LPSECURITY_ATTRIBUTES lbSecurityAttributes,
    _Out_ PHKEY phkResult,
    _Out_opt_ LPDWORD lpdwDisposition);
extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegDeleteKeyEx(_In_ HKEY hkey, _In_opt_ LPCWSTR lpSubKey, _In_ REGSAM samDesired, _In_ DWORD Reserved);
extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegSetValueEx(
    _In_ HKEY hkey,
    _In_opt_ LPCWSTR lpValueName,
    _In_opt_ DWORD Reserved,
    _In_ DWORD dwType,
    _In_ const BYTE * lpData,
    _In_ DWORD cbData);
extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegGetValue(
    _In_ HKEY hkey,
    _In_opt_ LPCWSTR lpSubKey,
    _In_opt_ LPCWSTR lpValue,
    _In_opt_ DWORD dwFlags,
    _Out_opt_ LPDWORD pdwType,
    _Out_opt_ PVOID pvData,
    _Inout_opt_ LPDWORD pcbData);

void Attach_All()
{
    DetourAttach(&(PVOID&)OldMessageBoxW, NewMessageBoxW);
    DetourAttach(&(PVOID&)OldMessageBoxA, NewMessageBoxA);
    DetourAttach(&(PVOID&)OldCreateFile, NewCreateFile);
    DetourAttach(&(PVOID&)OldDeleteFile, NewDeleteFile);
    DetourAttach(&(PVOID&)OldWriteFile, NewWriteFile);
    DetourAttach(&(PVOID&)OldReadFile, NewReadFile);
    DetourAttach(&(PVOID&)OldCloseHandle, NewCloseHandle);
    DetourAttach(&(PVOID&)OldCopyFile, NewCopyFile);
    DetourAttach(&(PVOID&)OldHeapCreate, NewHeapCreate);
    DetourAttach(&(PVOID&)OldHeapDestroy, NewHeapDestroy);
    DetourAttach(&(PVOID&)OldHeapAlloc, NewHeapAlloc);
    DetourAttach(&(PVOID&)OldHeapFree, NewHeapFree);
    DetourAttach(&(PVOID&)OldRegCreateKeyEx, NewRegCreateKeyEx);
    DetourAttach(&(PVOID&)OldRegDeleteKeyEx, NewRegDeleteKeyEx);
    DetourAttach(&(PVOID&)OldRegSetValueEx, NewRegSetValueEx);
    DetourAttach(&(PVOID&)OldRegGetValue, NewRegGetValue);
    DetourTransactionCommit();
}

void Detach_All()
{
    DetourDetach(&(PVOID&)OldMessageBoxW, NewMessageBoxW);
    DetourDetach(&(PVOID&)OldMessageBoxA, NewMessageBoxA);
    DetourDetach(&(PVOID&)OldCreateFile, NewCreateFile);
    DetourDetach(&(PVOID&)OldDeleteFile, NewDeleteFile);
    DetourDetach(&(PVOID&)OldWriteFile, NewWriteFile);
    DetourDetach(&(PVOID&)OldReadFile, NewReadFile);
    DetourDetach(&(PVOID&)OldCloseHandle, NewCloseHandle);
    DetourDetach(&(PVOID&)OldCopyFile, NewCopyFile);
    DetourDetach(&(PVOID&)OldHeapCreate, NewHeapCreate);
    DetourDetach(&(PVOID&)OldHeapDestroy, NewHeapDestroy);
    DetourDetach(&(PVOID&)OldHeapAlloc, NewHeapAlloc);
    DetourDetach(&(PVOID&)OldHeapFree, NewHeapFree);
    DetourDetach(&(PVOID&)OldRegCreateKeyEx, NewRegCreateKeyEx);
    DetourDetach(&(PVOID&)OldRegDeleteKeyEx, NewRegDeleteKeyEx);
    DetourDetach(&(PVOID&)OldRegSetValueEx, NewRegSetValueEx);
    DetourDetach(&(PVOID&)OldRegGetValue, NewRegGetValue);
    DetourTransactionCommit();
}

void WriteLog()
{
    char buf[100];
    sprintf_s(buf, "\n\n*********************************\n");
    WriteFile(hFile0, buf, strlen(buf) + 1, NULL, NULL);
    strcpy_s(buf, apilog.type);
    WriteFile(hFile0, buf, strlen(buf) + 1, NULL, NULL);
    sprintf_s(buf, "DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", apilog.st.wYear, apilog.st.wMonth, apilog.st.wDay, apilog.st.wHour, apilog.st.wMinute, apilog.st.wSecond, apilog.st.wMilliseconds);
    WriteFile(hFile0, buf, strlen(buf) + 1, NULL, NULL);
    for (int i = 0; i < apilog.argNum; i++)
    {
        WriteFile(hFile0, apilog.argName[i], strlen(apilog.argName[i]) + 1, NULL, NULL);
        WriteFile(hFile0, apilog.argVal[i], strlen(apilog.argVal[i]) + 1, NULL, NULL);
    }
    if (apilog.err)
    {
        WriteFile(hFile0, apilog.errormsg, strlen(apilog.errormsg) + 1, NULL, NULL);
        apilog.err = 0;
  
    }
    
    sprintf_s(buf, "*********************************\n");
    WriteFile(hFile0, buf, strlen(buf) + 1, NULL, NULL);
}


//
//MessageBoxAPI
//
extern "C" __declspec(dllexport)int WINAPI NewMessageBoxA(_In_opt_ HWND hWnd, _In_opt_ LPCSTR lpText, _In_opt_ LPCSTR lpCaption, _In_ UINT uType)
{
    /*printf("\n\n*********************************\n");
    printf("MessageBoxA Hooked\n");
    GetLocalTime(&st);
    printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
    printf("*********************************\n\n");*/

    strcpy_s(apilog.type, "MessageBoxA\n");
    apilog.argNum = 4;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hWnd:");
    sprintf_s(apilog.argName[1], "lpText:");
    sprintf_s(apilog.argName[2], "lpCaption:");
    sprintf_s(apilog.argName[3], "uType:");
    // 参数值
    sprintf_s(apilog.argVal[0], "%llx\n", hWnd);
    sprintf_s(apilog.argVal[1], "%s\n", lpText);
    sprintf_s(apilog.argVal[2], "%s\n", lpCaption);
    sprintf_s(apilog.argVal[3], "%llx\n", uType);

    WriteLog();

    return OldMessageBoxA(NULL, "new MessageBoxA", "Hooked", MB_OK);
}

extern "C" __declspec(dllexport)int WINAPI NewMessageBoxW(_In_opt_ HWND hWnd, _In_opt_ LPCWSTR lpText, _In_opt_ LPCWSTR lpCaption, _In_ UINT uType)
{
    /*printf("\n\n*********************************\n");
    printf("MessageBoxW Hooked\n");
    GetLocalTime(&st);
    printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
    printf("*********************************\n\n");*/

    strcpy_s(apilog.type, "MessageBoxW\n");
    apilog.argNum = 4;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hWnd:");
    sprintf_s(apilog.argName[1], "lpText:");
    sprintf_s(apilog.argName[2], "lpCaption:");
    sprintf_s(apilog.argName[3], "uType:");
    // 参数值
    sprintf_s(apilog.argVal[0], "%llx\n", hWnd);
    //WideCharToMultiByte();
    sprintf_s(apilog.argVal[1], "%s\n", lpText);
    sprintf_s(apilog.argVal[2], "%s\n", lpCaption);
    sprintf_s(apilog.argVal[3], "%llx\n", uType);

    WriteLog();

    return OldMessageBoxW(NULL, L"new MessageBoxW", L"Hooked", MB_OK);
}

//
//FileAPI
//
extern "C" __declspec(dllexport)HANDLE WINAPI NewCreateFile(LPCTSTR lpFileName, DWORD dwDesiredAccess, DWORD dwShareMode, LPSECURITY_ATTRIBUTES lpSecurityAttributes, DWORD dwCreationDisposition, DWORD dwFlagsAndAttributes, HANDLE hTemplateFile)
{
    /*printf("\n\n*********************************\n");
    printf("CreateFile Hooked\n");
    GetLocalTime(&st);
    printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
    int num = WideCharToMultiByte(CP_OEMCP, NULL, lpFileName, -1, NULL, 0, NULL, FALSE);
    char* pchar = new char[num];
    WideCharToMultiByte(CP_OEMCP, NULL, lpFileName, -1, pchar, num, NULL, FALSE);

    std::cout << "lpFileName:" << pchar << std::endl;
    std::cout << "dwDesiredAccess:0x" << std::hex << dwDesiredAccess << std::endl;
    std::cout << "dwShareMode:0x" << std::hex << dwShareMode << std::endl;
    std::cout << "lpSecurityAttributes:0x" << std::hex << lpSecurityAttributes << std::endl;
    std::cout << "dwCreationDisposition:0x" << std::hex << dwCreationDisposition << std::endl;
    std::cout << "dwFlagsAndAttributes:0x" << std::hex << dwFlagsAndAttributes << std::endl;
    std::cout << "hTemplateFile:0x" << std::hex << hTemplateFile << std::endl;

    printf("*********************************\n\n");
    */
    HANDLE fileHandle = NULL;
    fileHandle = OldCreateFile(lpFileName, dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
    if (!Share_Flag)return fileHandle;
    if (fileHandle == hFile0)
    {
        return fileHandle;
    }
    if (File_Map.count(fileHandle) == 0)
    {
        File_Map[fileHandle] = 1;
    }
    // CloseHandle(fileHandle);

    strcpy_s(apilog.type, "CreateFile\n");
    apilog.argNum = 7;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "lpFileName:");
    sprintf_s(apilog.argName[1], "dwDesiredAccess:0x");
    sprintf_s(apilog.argName[2], "dwShareMode:0x");
    sprintf_s(apilog.argName[3], "lpSecurityAttributes:0x");
    sprintf_s(apilog.argName[4], "dwCreationDisposition:0x");
    sprintf_s(apilog.argName[5], "dwFlagsAndAttributes:0x");
    sprintf_s(apilog.argName[6], "hTemplateFile:0x");
    // 参数值
    int num = WideCharToMultiByte(CP_OEMCP, NULL, lpFileName, -1, NULL, 0, NULL, FALSE);
    char* pchar = new char[num];
    WideCharToMultiByte(CP_OEMCP, NULL, lpFileName, -1, pchar, num, NULL, FALSE);
    sprintf_s(apilog.argVal[0], "%s\n", pchar);
    sprintf_s(apilog.argVal[1], "%llx\n", dwDesiredAccess);
    sprintf_s(apilog.argVal[2], "%llx\n", dwShareMode);
    sprintf_s(apilog.argVal[3], "%llx\n", lpSecurityAttributes);
    sprintf_s(apilog.argVal[4], "%llx\n", dwCreationDisposition);
    sprintf_s(apilog.argVal[5], "%llx\n", dwFlagsAndAttributes);
    sprintf_s(apilog.argVal[6], "%llx\n", hTemplateFile);

   
    //实现打开文件所在目录下的文件夹数量
    //LRL:我的理解为打开的文件所在的目录下是否还有文件夹
    HANDLE hFind = INVALID_HANDLE_VALUE;    //hFind保存findFirstFile返回的搜索句柄
    long long folder_num = 0;
    WIN32_FIND_DATA ffd;        //用来保存findFirstFile函数得到的文件属性对象
    WCHAR path[1000];
    Share_Flag = 0;             //用来使得GetFinalPathNameByHandle函数调用的CreateFile和CloseHandle不输出
    GetFinalPathNameByHandle(fileHandle, path, 1000, VOLUME_NAME_DOS); //只有用DOS值可以，用NONE值如果传递的不是绝对路径就会失败，但是DOS值会多输入非我们所需的路径，原因暂时
    Share_Flag = 1;
    if (!wcscmp(path, L""))
    {
        WriteLog();
        return fileHandle;       //上述输入非我们所需的路径的解决方法，原因未知
    }
    //wcscpy_s(path, lpFileName);
    // printf("Path:%ws\n", path+4);
    PathCchRemoveFileSpec(path, 1000);
    wcscat_s(path, L"\\*");     //将path变量设置为搜索该路径下的所有文件的路径名字
    if (!wcscmp(path, L"\\*")) {
        WriteLog();
        return fileHandle;       //上述输入非我们所需的路径的解决方法，原因未知
    }
    // printf("Path2:%ws\n", path+4);
    hFind = FindFirstFile(path, &ffd);
    if (hFind == INVALID_HANDLE_VALUE) {
        WriteLog();
        return fileHandle;       
    }
    //判断搜索到的文件是否为目录
    do {
        if (ffd.dwFileAttributes == FILE_ATTRIBUTE_DIRECTORY) {
            folder_num++;
            // printf("%ws\n", ffd.cFileName);
        }
    } while (FindNextFile(hFind, &ffd) != 0);
    FindClose(hFind); 
    apilog.err = 1;
    sprintf_s(apilog.errormsg,"Warning:Exiting multiple folders!\nfoldernum:%llx\n" ,folder_num );
    WriteLog();
    //return OldCreateFile(lpFileName, dwDesiredAccess, dwShareMode, lpSecurityAttributes, dwCreationDisposition, dwFlagsAndAttributes, hTemplateFile);
    return fileHandle;
}

extern "C" __declspec(dllexport)BOOL WINAPI NewDeleteFile(LPCTSTR lpFileName)
{
    /*printf("\n\n*********************************\n");
    printf("DeleteFile Hooked\n");
    GetLocalTime(&st);
    printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);
    int num = WideCharToMultiByte(CP_OEMCP, NULL, lpFileName, -1, NULL, 0, NULL, FALSE);
    char* pchar = new char[num];
    WideCharToMultiByte(CP_OEMCP, NULL, lpFileName, -1, pchar, num, NULL, FALSE);

    std::cout << "lpFileName:" << pchar << std::endl;

    printf("*********************************\n\n");
    */
    strcpy_s(apilog.type, "DeleteFile\n");
    apilog.argNum = 1;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "lpFileName:");
    // 参数值
    int num = WideCharToMultiByte(CP_OEMCP, NULL, lpFileName, -1, NULL, 0, NULL, FALSE);
    char* pchar = new char[num];
    WideCharToMultiByte(CP_OEMCP, NULL, lpFileName, -1, pchar, num, NULL, FALSE);
    sprintf_s(apilog.argVal[0], "%llx\n", pchar);

    WriteLog();
    return OldDeleteFile(lpFileName);
}

extern "C" __declspec(dllexport)BOOL WINAPI NewReadFile(HANDLE hFile, LPVOID lpBuffer, DWORD nNumberOfBytesToRead, LPDWORD lpNumberOfBytesRead, LPOVERLAPPED lpOverlapped)
{
    /*printf("\n\n*********************************\n");
    printf("ReadFile Hooked\n");
    GetLocalTime(&st);
    printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

    std::cout << "hFile:" << std::hex << hFile << std::endl;
    std::cout << "lpBuffer:" << std::hex << lpBuffer << std::endl;
    std::cout << "nNumberOfBytesToRead:" << std::hex << nNumberOfBytesToRead << std::endl;
    std::cout << "lpNumberOfBytesRead:" << std::hex << lpNumberOfBytesRead << std::endl;
    std::cout << "lpOverlapped:" << std::hex << lpOverlapped << std::endl;

    printf("*********************************\n\n");
    */
    strcpy_s(apilog.type, "ReadFile\n");
    apilog.argNum = 5;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hFile:");
    sprintf_s(apilog.argName[1], "lpBuffer:");
    sprintf_s(apilog.argName[2], "nNumberOfBytesToRead:");
    sprintf_s(apilog.argName[3], "lpNumberOfBytesRead:");
    sprintf_s(apilog.argName[4], "lpOverlapped:");
    // 参数值
    sprintf_s(apilog.argVal[0], "%llx\n", hFile);
    sprintf_s(apilog.argVal[1], "%llx\n", lpBuffer);
    sprintf_s(apilog.argVal[2], "%llx\n", nNumberOfBytesToRead);
    sprintf_s(apilog.argVal[3], "%llx\n", lpNumberOfBytesRead);
    sprintf_s(apilog.argVal[4], "%llx\n", lpOverlapped);

    WriteLog();
    return OldReadFile(hFile, lpBuffer, nNumberOfBytesToRead, lpNumberOfBytesRead, lpOverlapped);
}

//写操作代码
extern "C" __declspec(dllexport)BOOL WINAPI NewWriteFile(HANDLE hFile, LPCVOID lpBuffer, DWORD nNumberOfBytesToWrite, LPDWORD lpNumberOfBytesWritten, LPOVERLAPPED lpOverlapped)
{
    /*printf("\n\n*********************************\n");
    printf("WriteFile Hooked\n");
    GetLocalTime(&st);
    printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

    std::cout << "hFile:" << std::hex << hFile << std::endl;
    std::cout << "lpBuffer:" << std::hex << lpBuffer << std::endl;
    std::cout << "nNumberOfBytesToWrite:" << std::hex << nNumberOfBytesToWrite << std::endl;
    std::cout << "lpNumberOfBytesWritten:" << std::hex << lpNumberOfBytesWritten << std::endl;
    std::cout << "lpOverlapped:" << std::hex << lpOverlapped << std::endl;

    printf("*********************************\n\n");
    */
    if (hFile == hFile0)
    {
        return OldWriteFile(hFile, lpBuffer, nNumberOfBytesToWrite, lpNumberOfBytesWritten, lpOverlapped);
    }
    if (File_Map.count(hFile) == 0)
    {
        return OldWriteFile(hFile, lpBuffer, nNumberOfBytesToWrite, lpNumberOfBytesWritten, lpOverlapped);
    }


    //for (int i = 0; i < Filenum; i++) {
        //if (hFile == FileCreated[i]) {

    strcpy_s(apilog.type, "WriteFile\n");
    apilog.argNum = 5;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hFile:");
    sprintf_s(apilog.argName[1], "lpBuffer:");
    sprintf_s(apilog.argName[2], "nNumberOfBytesToWrite:");
    sprintf_s(apilog.argName[3], "lpNumberOfBytesWritten:");
    sprintf_s(apilog.argName[4], "lpOverlapped:");
    // 参数值
    sprintf_s(apilog.argVal[0], "%llx\n", hFile);
    sprintf_s(apilog.argVal[1], "%llx\n", lpBuffer);
    sprintf_s(apilog.argVal[2], "%llx\n", nNumberOfBytesToWrite);
    sprintf_s(apilog.argVal[3], "%llx\n", lpNumberOfBytesWritten);
    sprintf_s(apilog.argVal[4], "%llx\n", lpOverlapped);

    WriteLog();
    //}
//}
//实现判断是否修改可执行文件
//LRL：我认为可能就是判断传入WriteFile的参数是否为相应的高危的文件后缀，如果是的话，则报相应的错误。
    WCHAR path[1000];
    PCWSTR* ext = (PCWSTR*)malloc(sizeof(PCWSTR));        //指向path中拓展名的指针
    Share_Flag = 0;
    GetFinalPathNameByHandle(hFile, path, 1000, VOLUME_NAME_DOS);
    Share_Flag = 1;
    if (ext != NULL) {
        PathCchFindExtension(path, 1000, ext);
        //printf("%ws\n", *ext);
        if (wcscmp(*ext, L".exe")==0 || wcscmp(*ext, L".dll")==0 || wcscmp(*ext, L".ocx")==0) {
            printf("Warning:The process is writing executable file!\n");
            printf("The file name:%ws\n", path + 4);
        }
    }
    return OldWriteFile(hFile, lpBuffer, nNumberOfBytesToWrite, lpNumberOfBytesWritten, lpOverlapped);
}

extern "C" _declspec(dllexport)BOOL WINAPI NewCloseHandle(HANDLE hObject)
{
    /*printf("\n\n*********************************\n");
    printf("CloseHandle Hooked\n");
    GetLocalTime(&st);
    printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

    std::cout << "hFile:" << std::hex << hObject << std::endl;

    printf("*********************************\n\n");
    */
    if (Share_Flag)    return OldCloseHandle(hObject);
    if (File_Map.count(hObject) == 0)
    {
        return OldCloseHandle(hObject);
    }

    strcpy_s(apilog.type, "CloseHandle\n");
    apilog.argNum = 1;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hObject:");
    // 参数值
    sprintf_s(apilog.argVal[0], "%llx\n", hObject);

    WriteLog();
    return OldCloseHandle(hObject);
}
//文件异常分析之文件自我复制
//本思路：调用CopyFile   思路2：APP中如果调用自己，即可能发生自我复制
extern "C" _declspec(dllexport)BOOL WINAPI NewCopyFile(LPCTSTR lpExistingFileName, LPCTSTR lpNewFileName, BOOL bFailIfExists)
{
    strcpy_s(apilog.type, "CopyFile Catch!\n");
    apilog.argNum = 2;
    GetLocalTime(&(apilog.st));

    int num = WideCharToMultiByte(CP_OEMCP, NULL, lpExistingFileName, -1, NULL, 0, NULL, FALSE);
    char* pchar = new char[num];
    WideCharToMultiByte(CP_OEMCP, NULL, lpExistingFileName, -1, pchar, num, NULL, FALSE);

    int num2 = WideCharToMultiByte(CP_OEMCP, NULL, lpNewFileName, -1, NULL, 0, NULL, FALSE);
    char* pchar2 = new char[num2];
    WideCharToMultiByte(CP_OEMCP, NULL, lpNewFileName, -1, pchar2, num2, NULL, FALSE);

    sprintf_s(apilog.argName[0], "lpExistingFileName:");
    sprintf_s(apilog.argName[1], "lpNewFileName:");
    // 参数值
    sprintf_s(apilog.argVal[0], "%s\n", pchar);
    sprintf_s(apilog.argVal[1], "%s\n", pchar2);

    WriteLog();

    return OldCopyFile(lpExistingFileName, lpNewFileName, bFailIfExists);
}
//
//HeapAPI
//
extern "C" __declspec(dllexport)HANDLE WINAPI NewHeapCreate(DWORD fIOoptions, SIZE_T dwInitialSize, SIZE_T dwMaximumSize)
{
    HANDLE hHeap = OldHeapCreate(fIOoptions, dwInitialSize, dwMaximumSize);
    if (hHeapMap.count(hHeap) == 0)
    {
        hHeapMap[hHeap] = 1;
    }

    strcpy_s(apilog.type, "HeapCreate\n");
    apilog.argNum = 4;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "fIOoptions:");
    sprintf_s(apilog.argName[1], "dwInitialSize:");
    sprintf_s(apilog.argName[2], "dwMaximumSize:");
    sprintf_s(apilog.argName[3], "hHeap:");
    // 参数值
    sprintf_s(apilog.argVal[0], "%llx\n", fIOoptions);
    sprintf_s(apilog.argVal[1], "%llx\n", dwInitialSize);
    sprintf_s(apilog.argVal[2], "%llx\n", dwMaximumSize);
    sprintf_s(apilog.argVal[3], "%llx\n", hHeap);

    WriteLog();

    return hHeap;
}

extern "C" __declspec(dllexport)BOOL WINAPI NewHeapDestroy(HANDLE hHeap)
{
    /*printf("\n\n*********************************\n");
    printf("HeapDestory Hooked\n");
    GetLocalTime(&st);
    printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

    std::cout << "hHeap:" << hHeap << std::endl;

    printf("*********************************\n\n");*/
    strcpy_s(apilog.type, "HeapDestroy\n");
    apilog.argNum = 1;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hHeap:");

    sprintf_s(apilog.argVal[0], "%llx\n", hHeap);
    WriteLog();

    return OldHeapDestroy(hHeap);
}

extern "C" __declspec(dllexport)LPVOID WINAPI NewHeapAlloc(HANDLE hHeap, DWORD dwFlags, SIZE_T dwBytes)
{
    //Detach_All();

    if (hHeapMap.count(hHeap) == 0)
    {
        return OldHeapAlloc(hHeap, dwFlags, dwBytes);
    }
    //统计Alloc的地址
    LPVOID Alloc_Addr = OldHeapAlloc(hHeap, dwFlags, dwBytes);
    if (Alloc_Addr_Map.count(Alloc_Addr) == 0)
    {
        Alloc_Addr_Map[Alloc_Addr] = 1;
    }

    strcpy_s(apilog.type, "HeapAlloc\n");
    apilog.argNum = 4;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hHeap:");
    sprintf_s(apilog.argName[1], "dwFlags:");
    sprintf_s(apilog.argName[2], "dwBytes:");
    sprintf_s(apilog.argName[3], "lpMem:");

    sprintf_s(apilog.argVal[0], "%llx\n", hHeap);
    sprintf_s(apilog.argVal[1], "%llx\n", dwFlags);
    sprintf_s(apilog.argVal[2], "%llx\n", dwBytes);
    sprintf_s(apilog.argVal[3], "%llx\n", Alloc_Addr);

    WriteLog();

    //printf("\n\n*********************************\n");
    //printf("HeapAlloc Hooked\n");
    //GetLocalTime(&st);
    //printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

    //std::cout << "hHeap:" << hHeap << std::endl;
    //std::cout << "dwFlags:0x" << std::hex << dwFlags << std::endl;
    //std::cout << "dwBytes:0x" << std::hex << dwBytes << std::endl;

    //printf("*********************************\n\n");

    //Attach_All();
    return OldHeapAlloc(hHeap, dwFlags, dwBytes);
}

extern "C" __declspec(dllexport)BOOL WINAPI NewHeapFree(HANDLE hHeap, DWORD dwFlags, LPVOID lpMem)
{
    /*printf("\n\n*********************************\n");
    printf("HeapFree Hooked\n");
    GetLocalTime(&st);
    printf("DLL output: %d-%d-%d %02d:%02d:%02d:%03d\n", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond, st.wMilliseconds);

    std::cout << "hHeap:" << hHeap << std::endl;
    std::cout << "dwFlags:" << std::hex << dwFlags << std::endl;
    std::cout << "lpMem:" << lpMem << std::endl;*/

    strcpy_s(apilog.type, "HeapFree\n");
    apilog.argNum = 3;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hHeap:");
    sprintf_s(apilog.argName[1], "dwFlags:");
    sprintf_s(apilog.argName[2], "lpMem:");

    sprintf_s(apilog.argVal[0], "%llx\n", hHeap);
    sprintf_s(apilog.argVal[1], "%llx\n", dwFlags);
    sprintf_s(apilog.argVal[2], "%llx\n", lpMem);
    WriteLog();

    //free的指针实际地址与alloc的指针实际地址相差32（0x20）字节
    LPVOID lpMem20;
    lpMem20 = (LPVOID)((uint64_t)lpMem - 0x20);
    //std::cout << lpMem20 << std::endl;

    //检测堆申请与释放是否一致
    if ((Alloc_Addr_Map.count(lpMem20) == 0) /* && lpMem != NULL*/)
    {
        apilog.err = 1;
        sprintf_s(apilog.errormsg,"ERROR! An unallocated pointer %llx has been freed!\n", lpMem);
    }
    else
    {
        if (Freed_Addr_Map.count(lpMem))
        {
            Freed_Addr_Map[lpMem]++;
        }
        else Freed_Addr_Map[lpMem] = 1;

        //std::cout << Freed_addr_Map[lpMem] << std::endl;

        if ((Alloc_Addr_Map[lpMem20] < Freed_Addr_Map[lpMem])  /* && lpMem != NULL*/)
        {
            apilog.err = 1;
            sprintf_s(apilog.errormsg,"ERROR! A freed pointer %llx has been freed again!\n", lpMem);
        }
    }


    return OldHeapFree(hHeap, dwFlags, lpMem);
}

//
//Register API
//
extern "C" __declspec(dllexport)  LSTATUS WINAPI NewRegCreateKeyEx(_In_ HKEY hkey, _In_ LPCWSTR lpSubKey, _In_ DWORD Reserved, _In_opt_ LPWSTR lpClass, _In_ DWORD dwOptions, _In_ REGSAM samDesired, _In_opt_ const LPSECURITY_ATTRIBUTES lbSecurityAttributes, _Out_ PHKEY phkResult, _Out_opt_ LPDWORD lpdwDisposition)
{
    LSTATUS res = OldRegCreateKeyEx(hkey, lpSubKey, Reserved, lpClass, dwOptions, samDesired, lbSecurityAttributes, phkResult, lpdwDisposition);
    strcpy_s(apilog.type, "RegCreateKeyEx\n");
    apilog.argNum = 9;
    GetLocalTime(&(apilog.st));
    //输入相应的变量值（但暂时只是单纯输出，没有转化为有意义的字符)
    sprintf_s(apilog.argName[0], "hkey:");
    sprintf_s(apilog.argName[1], "lpSubKey:");
    sprintf_s(apilog.argName[2], "Reserved:");
    sprintf_s(apilog.argName[3], "lpClass:");
    sprintf_s(apilog.argName[4], "dwOptions:");
    sprintf_s(apilog.argName[5], "samDesired:");
    sprintf_s(apilog.argName[6], "lbSecurityAttributes:");
    sprintf_s(apilog.argName[7], "phkResult:");
    sprintf_s(apilog.argName[8], "lpdwDisposition:");

    sprintf_s(apilog.argVal[0], "%llx\n", hkey);
    sprintf_s(apilog.argVal[1], "%llx\n", lpSubKey);
    sprintf_s(apilog.argVal[2], "%llx\n", Reserved);
    sprintf_s(apilog.argVal[3], "%llx\n", lpClass);
    sprintf_s(apilog.argVal[4], "%llx\n", dwOptions);
    sprintf_s(apilog.argVal[5], "%llx\n", samDesired);
    sprintf_s(apilog.argVal[6], "%llx\n", lbSecurityAttributes);
    sprintf_s(apilog.argVal[7], "%llx\n", phkResult);
    sprintf_s(apilog.argVal[8], "%llx\n", lpdwDisposition);

   

    //----以下注释是测试注入的代码-----
    //char转换为LPCWSTR
    //const char* szStr = "CAJViewer 7.3u\\Hook";
    //WCHAR wszClassName[256];
    //memset(wszClassName, 0, sizeof(wszClassName));
    //MultiByteToWideChar(CP_ACP, 0, szStr, strlen(szStr) + 1, wszClassName,
    //sizeof(wszClassName) / sizeof(wszClassName[0]));

    //HKEY hkey_test;
   //DWORD dwDisposition = REG_OPENED_EXISTING_KEY;
    //return OldRegCreateKeyEx(HKEY_CURRENT_USER,wszClassName ,0,NULL, REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&hkey_test,NULL);
    //————————————————————————
    //printf("attempting to create a registry key %llx\n", hkey);
   
    std::wstring subKeyStr(lpSubKey ? lpSubKey : L"");
    if ((hkey == HKEY_LOCAL_MACHINE && subKeyStr.find(L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run") != std::wstring::npos) ||
        (hkey == HKEY_CURRENT_USER && subKeyStr.find(L"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run") != std::wstring::npos)) {
        apilog.err = 1;
        strcpy_s(apilog.errormsg, "Attempt to create an autostart regkey\n");
         danger_hkey[*phkResult] = 1;
    }
        WriteLog();

    return res;
}

extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegDeleteKeyEx(_In_ HKEY hkey, _In_opt_ LPCWSTR lpSubKey, _In_ REGSAM samDesired, _In_ DWORD Reserved)
{
    //----以下注释是测试注入的代码-----
    //const char* szStr = "CAJViewer 7.3u\\Hook";
    //WCHAR wszClassName[256];
    //memset(wszClassName, 0, sizeof(wszClassName));
    //MultiByteToWideChar(CP_ACP, 0, szStr, strlen(szStr) + 1, wszClassName,
    //    sizeof(wszClassName) / sizeof(wszClassName[0]));
    //return OldRegDeleteKeyEx(HKEY_CURRENT_USER, wszClassName, KEY_WOW64_64KEY,0);
    //————————————————————————


    //输入相应的变量值（但暂时只是单纯输出，没有转化为有意义的字符)
    strcpy_s(apilog.type, "RegDeleteKeyEx\n");
    apilog.argNum = 4;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hkey:");
    sprintf_s(apilog.argName[1], "lpSubKey:");
    sprintf_s(apilog.argName[2], "samDesired:");
    sprintf_s(apilog.argName[3], "Reserved:");

    sprintf_s(apilog.argVal[0], "%llx\n", hkey);
    sprintf_s(apilog.argVal[1], "%llx\n", lpSubKey);
    sprintf_s(apilog.argVal[2], "%llx\n", samDesired);
    sprintf_s(apilog.argVal[3], "%llx\n", Reserved);

    WriteLog();
    //printf("attempting to delete a registry key %llx\n", hkey);

    return OldRegDeleteKeyEx(hkey, lpSubKey, samDesired, Reserved);
}

extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegSetValueEx(_In_ HKEY hkey, _In_opt_ LPCWSTR lpValueName, _In_opt_ DWORD Reserved, _In_ DWORD dwType, _In_ const BYTE * lpData, _In_ DWORD cbData)
{
    //----以下注释是测试注入的代码-----
    //const char* szStr = "CAJViewer 7.3u\\Config";
    //WCHAR wszClassName[256];
    //memset(wszClassName, 0, sizeof(wszClassName));
    //MultiByteToWideChar(CP_ACP, 0, szStr, strlen(szStr) + 1, wszClassName,
    //    sizeof(wszClassName) / sizeof(wszClassName[0]));
    //HKEY hkey_test;
    //OldRegCreateKeyEx(HKEY_CURRENT_USER, wszClassName, 0, NULL,REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey_test, NULL);
    //const char* test_value = "hook";
    //WCHAR wszTestValue[256];
    //memset(wszTestValue, 0, sizeof(wszTestValue));
    //MultiByteToWideChar(CP_ACP, 0, test_value, strlen(test_value) + 1, wszTestValue,
    //    sizeof(wszTestValue) / sizeof(wszTestValue[0]));
    //return OldRegSetValueEx(hkey, wszTestValue, 0, REG_SZ, NULL, 0);
    //————————————————————————

    //输入相应的变量值（但暂时只是单纯输出，没有转化为有意义的字符)
    strcpy_s(apilog.type, "RegSetValueEx\n");
    apilog.argNum = 6;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hkey:");
    sprintf_s(apilog.argName[1], "lpValueName:");
    sprintf_s(apilog.argName[2], "Reserved:");
    sprintf_s(apilog.argName[3], "dwType:");
    sprintf_s(apilog.argName[4], "lpData:");
    sprintf_s(apilog.argName[5], "cbData:");

    sprintf_s(apilog.argVal[0], "%llx\n", hkey);
    sprintf_s(apilog.argVal[1], "%llx\n", lpValueName);
    sprintf_s(apilog.argVal[2], "%llx\n", Reserved);
    sprintf_s(apilog.argVal[3], "%llx\n", dwType);
    sprintf_s(apilog.argVal[4], "%llx\n", lpData);
    sprintf_s(apilog.argVal[5], "%llx\n", cbData);
    
    if (danger_hkey[hkey]==1) {
        apilog.err = 1;
        strcpy_s(apilog.errormsg, "Attempt to modify an autostart regkey\n");
    }
    WriteLog();
    return OldRegSetValueEx(hkey, lpValueName, Reserved, dwType, lpData, cbData);

}

extern "C" __declspec(dllexport) LSTATUS WINAPI NewRegGetValue(_In_ HKEY hkey, _In_opt_ LPCWSTR lpSubKey, _In_opt_ LPCWSTR lpValue, _In_opt_ DWORD dwFlags, _Out_opt_ LPDWORD pdwType, _Out_opt_ PVOID pvData, _Inout_opt_ LPDWORD pcbData)
{


    //----以下注释是测试注入的代码-----
    //return OldRegGetValue(HKEY_CURRENT_USER, L"CAJViewer 7.3u\\Config", L"hook", RRF_RT_ANY,pdwType,pvData,pcbData );
    //————————————————————————

    //输入相应的变量值（但暂时只是单纯输出，没有转化为有意义的字符)

    strcpy_s(apilog.type, "RegGetValue\n");
    apilog.argNum = 7;
    GetLocalTime(&(apilog.st));

    sprintf_s(apilog.argName[0], "hkey:");
    sprintf_s(apilog.argName[1], "lpSubKey:");
    sprintf_s(apilog.argName[2], "lpValue:");
    sprintf_s(apilog.argName[3], "dwFlags:");
    sprintf_s(apilog.argName[4], "pdwType:");
    sprintf_s(apilog.argName[5], "pvData:");
    sprintf_s(apilog.argName[6], "pcbData:");

    sprintf_s(apilog.argVal[0], "%llx\n", hkey);
    sprintf_s(apilog.argVal[1], "%llx\n", lpSubKey);
    sprintf_s(apilog.argVal[2], "%llx\n", lpValue);
    sprintf_s(apilog.argVal[3], "%llx\n", dwFlags);
    sprintf_s(apilog.argVal[4], "%llx\n", pdwType);
    sprintf_s(apilog.argVal[5], "%llx\n", pvData);
    sprintf_s(apilog.argVal[6], "%llx\n", pcbData);

    WriteLog();
    //printf("attempting to get a registry value\n");

    return OldRegGetValue(hkey, lpSubKey, lpValue, dwFlags, pdwType, pvData, pcbData);

}

LPCSTR GetCurrentProcessName()
{
    HMODULE hMod = NULL;
    PCHAR pszBuf = NULL;
    DWORD dwRet = 0;

    hMod = GetModuleHandle(NULL);
    pszBuf = new CHAR[MAX_PATH];
    if (NULL == pszBuf)
    {
        return(NULL);
    }
    RtlZeroMemory(pszBuf, MAX_PATH);

    dwRet = GetModuleFileNameA((HMODULE)hMod, pszBuf, MAX_PATH);
    if (!dwRet)
    {
        delete[] pszBuf;
        return(NULL);
    }

    return pszBuf;
}

BOOL WINAPI DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved
)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    {
        DisableThreadLibraryCalls(hModule);
        DetourTransactionBegin();
        DetourUpdateThread(GetCurrentThread());
        pszBuf = GetCurrentProcessName();
        //std::cout <<"Exe Name Get: " << pszBuf << std::endl;
        Attach_All();
    }
    break;
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        DetourTransactionBegin();
        DetourUpdateThread(GetCurrentThread());
        Detach_All();
        break;
    }
    return TRUE;
}