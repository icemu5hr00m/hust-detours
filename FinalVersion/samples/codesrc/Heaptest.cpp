#include <iostream>
#include <cstdio>
#include <windows.h>

int main()
{
	HANDLE hHeap1 = HeapCreate(0, 0, 0);
	HeapDestroy(hHeap1);

	HANDLE hHeap = HeapCreate(0, 0, 0);
	LPVOID lp1 = HeapAlloc(hHeap, 0, 10);
	
	BOOL bb = HeapFree(hHeap, 0, lp1);
	HeapFree(hHeap, 0, NULL);

	//HeapFree(hHeap, 0, lp1);

	HeapDestroy(hHeap);
	
	return 0;
}